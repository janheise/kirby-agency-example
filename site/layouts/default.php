<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= css(url: 'assets/css/app.css') ?>
</head>

<body class="flex flex-col min-h-screen antialiased">
    <main class="flex-grow">
        <?= $slot ?>
    </main>

    <?= js(url: 'assets/js/app.js') ?>
</body>

</html>
