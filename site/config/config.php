<?php

use Beebmx\KirbyEnv;

KirbyEnv::load(path: './');

return [
    'debug' => env(key: 'SHOW_DEBUG_INFORMATION'),
    'panel' => [
        'install' => env(key: 'ALLOW_PANEL_INSTALLATION')
    ],
    'languages' => true,
    'date'  => [
        'handler' => 'strftime'
    ],
    'routes' => [
        [
            'pattern' => 'website-check-form',
            'action' => function () {
                $data = [
                    'url'  => get('url'),
                    'email' => get('email'),
                    'recipient' => get('recipient'),
                    'message'  => get('message'),
                    'privacy'  => get('privacy') ?? false,
                    'newsletter'  => get('newsletter') ?? false,
                ];

                try {
                    kirby()->email([
                        'template' => 'website-check',
                        'from' => $data['recipient'],
                        'replyTo' => $data['email'],
                        'to' => $data['recipient'],
                        'subject' => 'Bitte analysiert ' . esc($data['url']),
                        'data' => [
                            'url' => esc($data['url']),
                            'email' => esc($data['email']),
                            'message' => esc($data['message']),
                            'privacy' => esc($data['privacy']),
                            'newsletter' => esc($data['newsletter']),
                        ],
                    ]);
                } catch (Exception $error) {
                    if(option('debug')):
                        $alert['error'] = 'The form could not be sent: <strong>' . $error->getMessage() . '</strong>';
                    else:
                        $alert['error'] = 'The form could not be sent!';
                    endif;
                }

                if (empty($alert)) {
                    $success = true;
                    $data = [];
                }

                return json_encode([
                    'alert' => $alert ?? false,
                    'status' => $success ?? false,
                    'data' => $data ?? false,
                ]);
            },
            'method' => 'POST',
        ],
        [
            'pattern' => 'sitemap.xml',
            'action' => function () {
                $pages = site()->pages()->index();

                // fetch the pages to ignore from the config settings,
                // if nothing is set, we ignore the error page
                $ignore = kirby()->option('sitemap.ignore', ['error']);

                $content = snippet(
                    name: 'sitemap',
                    data: compact('pages', 'ignore'),
                    return: true
                );

                // return response with correct header type
                return new Kirby\Cms\Response($content, 'application/xml');
            }
        ],
        [
            'pattern' => 'sitemap',
            'action'  => function () {
                return go('sitemap.xml', 301);
            }
        ],
    ],
    'sitemap.ignore' => ['error'],
];
