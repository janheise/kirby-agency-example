<?php

namespace Deployer;

require 'recipe/common.php';
require 'contrib/rsync.php';

set(name: 'allow_anonymous_stats', value: false);

import(file: '.hosts.yml');

set(name: 'application', value: 'agency');
set(name: 'repository', value: 'git@gitlab.com:janheise/kirby-agency-example.git');

set('shared_files', ['.env', '.htaccess', 'site/config/.license']);
set('shared_dirs', ['site/accounts', 'site/sessions', 'content']);

set('rsync', [
    'exclude' => array_merge(
        get('shared_files'),
        get('shared_dirs'),
    ),
    'exclude-file' => false,
    'include' => [],
    'include-file' => false,
    'filter' => [],
    'filter-file' => false,
    'filter-perdir' => false,
    'flags' => 'az',
    'options' => ['delete'],
    'timeout' => 120,
]);
set(name: 'rsync_src', value: '.build');

task('failed', [
    'deploy:unlock',
]);

desc(title: 'Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:setup',
    'deploy:lock',
    'deploy:release',
    'rsync',
    'deploy:shared',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'deploy:cleanup',
    'deploy:success',
]);

after(task: 'deploy:failed', do: 'failed');
